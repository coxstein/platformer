﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour, IEnemy, IHitBox
{
    public void RegisterEnemy()
    {
        GameManager manager = FindObjectOfType<GameManager>();
        manager.Enemies.Add(this);
    }

    [SerializeField] private int health = 1;

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
}

    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        print("Enemy Died");
    }

    private void Awake()
    {
        new Vector3(0f, 0f, 0f);

        RegisterEnemy();
    }
}
using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField] private Transform player;
    private Transform camTransform;
    
    private void Start()
    {
        camTransform = gameObject.transform;
    }
     
    private void Update ()
    {
        var camPosition = camTransform.position;
        gameObject.transform.position = new Vector3(player.position.x, camPosition.y,  camPosition.z);
    }
}
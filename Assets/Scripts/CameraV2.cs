using UnityEngine;

public class CameraV2 : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float leftBound;
    [SerializeField] private float rightBound;
    
    private Transform camTransform;
    
    private void Start()
    {
        camTransform = gameObject.transform;
    }
     
    private void Update ()
    {
        var position = target.position;
        position.y = transform.position.y;
        position.z = transform.position.z;
        
        position.x = Mathf.Lerp(transform.position.x, target.position.x, Time.deltaTime * 5f);
        position.x = Mathf.Clamp(position.x, leftBound, rightBound);
        
        transform.position = position;

    }
}
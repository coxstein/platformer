﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Coin : MonoBehaviour
{
    [SerializeField] private Animator animator; 
    
    private Transform canvasCoinImage;
    private Text canvasCoinText;
    private Coroutine moveUpCoroutine;
    private Player player;
    private static readonly int Rotate = Animator.StringToHash("Start");

    private void Start()
    {
        canvasCoinImage = GameObject.Find("CoinImage").GetComponent<Transform>();
        canvasCoinText = GameObject.Find("CoinText").GetComponent<Text>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (moveUpCoroutine == null && other.GetComponent<Player>())
        {
            player = other.GetComponent<Player>();
            moveUpCoroutine = StartCoroutine(MoveToCoinBalance());
        }
    }

    private IEnumerator MoveToCoinBalance()
    {
        animator.SetTrigger(Rotate);

        float y = canvasCoinImage.transform.position.y;
        while (transform.position.y < y)
        {
            var targetToMove = canvasCoinImage.transform.position;
            float step = 100 * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetToMove, step);
            yield return null;
        }

        player.Coins++;
        canvasCoinText.text = player.Coins.ToString();
        Destroy(gameObject);
    }
}
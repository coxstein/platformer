using System.Collections;
using System.Linq;
using UnityEngine;

public class CoinCube : MonoBehaviour
{
    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private int coinsAmount = 5;

    private void Awake()
    {
        particleSystem.Stop();
        var main = particleSystem.main;
        main.maxParticles = coinsAmount;
        particleSystem.GetComponent<ParticleToCoinTransformer>().ParticlesToProcess = coinsAmount;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>())
        {
            particleSystem.transform.parent = null;
            gameObject.GetComponents<BoxCollider2D>().ToList().Find(it => !it.isTrigger).enabled = false;
            StartCoroutine(ScaleAndDestroy(0.3f));
        }
        particleSystem.Play();
    }

    private IEnumerator ScaleAndDestroy(float time)
    {
        Vector3 originalScale = transform.localScale;
        var currentTime = 0.0f;
        while (currentTime <= time)
        {
            transform.localScale = Vector3.Lerp(originalScale, Vector3.zero, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
    }
}
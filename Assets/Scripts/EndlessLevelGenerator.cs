using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;
using Random = UnityEngine.Random;

public class EndlessLevelGenerator : MonoBehaviour
{
    [SerializeField] private Sprite startSprite;
    [SerializeField] private Sprite middleSprite;
    [SerializeField] private Sprite endSprite;
    [SerializeField] private GameObject target;
    [SerializeField] private PlatformGenerator platformGenerator;

    [SerializeField] private int platformsInOneTime = 7; // Odd number
    [SerializeField] private float maxDistanceBetweenPlatforms = 1f;
    [SerializeField] private int maxPlatformLength = 5;

    private const float PlatformSize = 1.28f;
    private const float PlatformMinY = -4f;
    private const float PlatformMaxY = 2.56f;

    private LinkedList<PlatformData> listPlatformData = new LinkedList<PlatformData>();
    private LinkedList<GameObject> platforms = new LinkedList<GameObject>();
    private LinkedListNode<PlatformData> playerPosition;

    private void Awake()
    {
        GeneratePlatform(Vector3.zero, Direction.Right);
        target.transform.position = listPlatformData.Last.Value.position + new Vector3(0.5f, 0f, 0f);
        playerPosition = listPlatformData.Last;
        for (var i = 0; i < platformsInOneTime / 2; i++)
        {
            GeneratePlatform(platforms.Last.Value.transform.position, Direction.Right);
            GeneratePlatform(platforms.First.Value.transform.position, Direction.Left);
        }
    }

    private void FixedUpdate()
    {
        if (target.transform.position.x > playerPosition.Next.Value.position.x)
        {
            var platformData = playerPosition;
            for (int i = 0; i <= platformsInOneTime / 2; i++)
            {
                platformData = platformData.Next;
            }

            if (platformData == null)
            {
                GeneratePlatform(platforms.Last.Value.transform.position, Direction.Right);
            }
            else
            {
                var platform = platformGenerator.CreatePlatformFromAsset(platformData.Value);
                platforms.AddLast(platform);
            }

            playerPosition = playerPosition.Next;
            if (platforms.Count > platformsInOneTime)
            {
                Destroy(platforms.First.Value);
                platforms.RemoveFirst();
            }
        }

        if (target.transform.position.x <= playerPosition.Value.position.x)
        {
            var platformData = playerPosition;
            for (int i = 0; i <= platformsInOneTime / 2; i++)
            {
                platformData = platformData.Previous;
            }

            if (platformData == null)
            {
                GeneratePlatform(platforms.First.Value.transform.position, Direction.Left);
            }
            else
            {
                var platform = platformGenerator.CreatePlatformFromAsset(platformData.Value);
                platforms.AddFirst(platform);
            }

            playerPosition = playerPosition.Previous;
            if (platforms.Count > platformsInOneTime)
            {
                Destroy(platforms.Last.Value);
                platforms.RemoveLast();
            }
        }
    }

    private void GeneratePlatform(Vector3 lastPlatformPosition, Direction direction)
    {
        var platformLength = Random.Range(2, maxPlatformLength);
        var distanceBetweenPlatforms = Random.Range(0.5f, maxDistanceBetweenPlatforms);

        float x;
        if (direction == Direction.Right)
        {
            var lastPlatformLength = listPlatformData.Last?.Value.length ?? 0;
            x = lastPlatformPosition.x + lastPlatformLength * PlatformSize + distanceBetweenPlatforms;
        }
        else
        {
            x = lastPlatformPosition.x - platformLength * PlatformSize - distanceBetweenPlatforms;
        }

        var yDirection = Random.Range(0, 2) * 2 - 1;
        var y = Mathf.Clamp(lastPlatformPosition.y + yDirection * PlatformSize, PlatformMinY, PlatformMaxY);
        
        var platformData = platformGenerator
            .CreatePlatformData(startSprite, middleSprite, endSprite, platformLength, new Vector3(x, y, 0f));
        var platform = platformGenerator.CreatePlatformFromAsset(platformData);

        if (direction == Direction.Right)
        {
            platforms.AddLast(platform);
            listPlatformData.AddLast(platformData);
        }
        else
        {
            platforms.AddFirst(platform);
            listPlatformData.AddFirst(platformData);
        }
    }
}

public enum Direction
{
    Left,
    Right
}
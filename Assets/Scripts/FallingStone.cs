using UnityEngine;

public class FallingStone : MonoBehaviour
{
    [SerializeField] private int damage = 5;

    private void FixedUpdate()
    {
        if (transform.position.y < -10f)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var other = collision.gameObject;
        if (other.GetComponent<IHitBox>() == null) return;
        other.transform.GetComponent<IHitBox>().Hit(damage);
    }
}
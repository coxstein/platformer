﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxAdapter : MonoBehaviour, IHitBox
{
    [SerializeField] private GameObject hitTarget;
    private IHitBox hitBox;

    /*private void Reset()
    {
        var hit = GetComponentInParent<IHitBox>();
        if (hit != null)
        {
            hitTarget = (hit as MonoBehaviour)?.gameObject;
        }
    }*/

    void Start()
    {
        hitBox = hitTarget.GetComponent<IHitBox>();
    }

    public int Health => hitBox.Health;

    public void Hit(int damage)
    {
        hitBox.Hit(damage);
    }

    public void Die()
    {
        hitBox.Die();
    }
}
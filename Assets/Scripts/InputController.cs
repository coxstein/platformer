﻿using System;
using System.Collections;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private const float DelayToPerformDoubleJump = 0.2f;
    private const float NormalJumpForce = 1f;
    private const float DoubleJumpForce = 1.25f;

    public static float HorizontalAxis;
    public static event Action<float> JumpAction;
    public static event Action<string> FireAction;
    public static event Action SwitchRun;

    private float jumpTimer;
    private Coroutine waitForJumpCoroutine;

    private void Start()
    {
        HorizontalAxis = 0f;
    }

    private void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            if (waitForJumpCoroutine == null)
            {
                waitForJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }

            jumpTimer = Time.time;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireAction?.Invoke("Fire1");
        }
        
        if (Input.GetButtonDown("Fire2"))
        {
            FireAction?.Invoke("Fire2");
        }

        if (Input.GetButtonDown("Run"))
        {
            SwitchRun?.Invoke();
        }
    }

    private void OnDestroy()
    {
        HorizontalAxis = 0f;
    }

    //time scale(to read)
    private IEnumerator WaitJump()
    {
        yield return new WaitForSeconds(0.2f);
        if (JumpAction != null)
        {
            float force = Time.time - jumpTimer < DelayToPerformDoubleJump ? DoubleJumpForce : NormalJumpForce;
            JumpAction.Invoke(force);
        }

        waitForJumpCoroutine = null;
    }
}
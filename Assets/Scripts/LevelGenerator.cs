using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private PlatformGenerator platformGenerator;
    [SerializeField] private List<PlatformDataBundle> platformBundles;

    private void Awake()
    {
        foreach (var platformBundle in platformBundles)
        {
            foreach (var platformData in platformBundle.platformDataBundle)
            {
                platformGenerator.CreatePlatformFromAsset(platformData);
            }
        }
    }
}
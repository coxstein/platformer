﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private bool isRandom;
    [SerializeField] private float minSpawnTime = 1f;
    [SerializeField] private float maxSpawnTime = 2f;

    private int currentIndex;

    // Start is called before the first frame update
    void  Start()
    {
        StartCoroutine(ObjectsCreatorProcess());
    }

    private IEnumerator ObjectsCreatorProcess()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minSpawnTime, maxSpawnTime));
            if (isRandom)
            {
                currentIndex = Random.Range(0, prefabs.Length);
            }
            
            GameObject obj = Instantiate(prefabs[currentIndex]);
            obj.transform.position = transform.position;

            if (isRandom)
            {
                continue;
            }
            currentIndex++;
            if (currentIndex >= prefabs.Length)
            {
                currentIndex = 0;
            }
        }
    }
}
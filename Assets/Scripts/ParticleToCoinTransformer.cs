using System.Collections.Generic;
using UnityEngine;

public class ParticleToCoinTransformer : MonoBehaviour
{
    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private GameObject coinPrefab;
    private List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();
    public int ParticlesToProcess { get; set; }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = particleSystem.GetCollisionEvents(other, collisionEvents);

        for (var i = 0; i < numCollisionEvents; i++)
        {
            Vector3 pos = collisionEvents[i].intersection;
            Instantiate(coinPrefab, pos, Quaternion.identity);
            ParticlesToProcess--;
        }

        if (ParticlesToProcess == 0)
        {
            Destroy(gameObject);
        }
    }
}
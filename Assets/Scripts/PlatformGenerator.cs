﻿using System.Collections.Generic;
using ScriptableObjects;
using UnityEditor;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    [SerializeField] private Transform platformParent;

    [Header("Select sprites for platform")] 
    [SerializeField] private Sprite startSprite;
    [SerializeField] private Sprite middleSprite;
    [SerializeField] private Sprite endSprite;

    [Header("Select any object on scene as start of next platform")] [SerializeField]
    private GameObject rootObject;

    [SerializeField, Range(2, 10)] private int length;

    [SerializeField] private List<GameObject> createdPlatforms = new List<GameObject>();
    [SerializeField] private List<PlatformData> createdPlatformsData = new List<PlatformData>();

    private const float PlatformSize = 1.28f;
    private const string SortingLayerName = "Platforms";
    private const string PlatformDataPath = "Assets/LevelData/PlatformData/GeneratedPlatformsBundle.asset";

    [ContextMenu("Generate Platform")]
    public void CreatePlatform()
    {
        var platformData = 
            CreatePlatformData(startSprite, middleSprite, endSprite, length, rootObject.transform.position);

        var platform = CreatePlatformFromAsset(platformData);
        createdPlatformsData.Add(platformData);
        createdPlatforms.Add(platform);
    }

    public PlatformData CreatePlatformData(Sprite start, Sprite middle, Sprite end, int length, Vector3 startPosition)
    {
        var platformData = ScriptableObject.CreateInstance<PlatformData>();
        platformData.startSprite = startSprite;
        platformData.middleSprite = middleSprite;
        platformData.endSprite = endSprite;
        platformData.length = length;
        platformData.position = startPosition;
        return platformData;
    }

    [ContextMenu("Save Platforms Data")]
    public void SavePlatformToAsset()
    {
        var multiplePlatformData = ScriptableObject.CreateInstance<PlatformDataBundle>();
        multiplePlatformData.platformDataBundle.AddRange(createdPlatformsData);
        
        var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(PlatformDataPath);
        AssetDatabase.CreateAsset(multiplePlatformData, assetPathAndName);
        foreach (var platformData in createdPlatformsData)
        {
            AssetDatabase.AddObjectToAsset(platformData, assetPathAndName);
        }
    }

    [ContextMenu("Destroy Created Platforms")]
    public void ClearPlatformGenerator()
    {
        foreach (var platform in createdPlatforms)
        {
            DestroyImmediate(platform);
        }

        createdPlatforms.Clear();
        createdPlatformsData.Clear();
    }

    public GameObject CreatePlatformFromAsset(PlatformData platformData)
    {
        var platform = new GameObject {name = $"Ground platform {platformData.length} segments"};
        var platformDataPosition = platformData.position;
        platform.transform.position = platformDataPosition;
        platform.transform.parent = platformParent;

        var platformDataLength = platformData.length;
        for (var i = 0; i < platformDataLength; i++)
        {
            var platformSegment = new GameObject {name = $"Platform Segment {i}"};
            platformSegment.transform.parent = platform.transform;

            var boxCollider2D = platformSegment.AddComponent<BoxCollider2D>();
            boxCollider2D.size = new Vector2(PlatformSize, PlatformSize);

            var spriteRenderer = platformSegment.AddComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = SortingLayerName;

            platformSegment.transform.position = new Vector3(platformDataPosition.x + i * PlatformSize,
                platformDataPosition.y, platformDataPosition.z);

            if (i == 0)
            {
                spriteRenderer.sprite = platformData.startSprite;
                continue;
            }

            if (i == platformDataLength - 1)
            {
                spriteRenderer.sprite = platformData.endSprite;
                continue;
            }

            spriteRenderer.sprite = platformData.middleSprite;
        }

        return platform;
    }
}
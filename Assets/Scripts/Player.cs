﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour, IPlayer, IHitBox
{
    [SerializeField] private int health = 1;

    public int Coins { get; set; } = 0;

    private PlayerWeapon[] weapons;
    
    private const string MeleeWeaponButton = "Fire1";
    private const string RangeWeaponButton = "Fire2";
    
    private static readonly int IsDead = Animator.StringToHash("IsDead");

    public void RegisterPlayer()
    {
        GameManager manager = FindObjectOfType<GameManager>();
        if (manager.Player == null)
        {
            manager.Player = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        print("Player Died");
        GetComponentInChildren<Animator>().SetBool(IsDead, true);
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Destroy(GetComponent<PlayerMovement>());
        OnDestroy();
        StartCoroutine(LoadMainMenu());
    }

    private void Awake()
    {
        RegisterPlayer();
        weapons = GetComponents<PlayerWeapon>();
        InputController.FireAction += Attack;
    }

    private void OnDestroy()
    {
        InputController.FireAction -= Attack;
    }

    private void Attack(string button)
    {
        foreach (var weapon in weapons)
        {
            if (button.Equals(MeleeWeaponButton))
            {
                weapon.SetDamage();
            }

            if (button.Equals(RangeWeaponButton))
            {
                weapon.ThrowProjectile();
            }
        }
    }

    private IEnumerator LoadMainMenu()
    {
        yield return new WaitForSeconds(1.5f);
        FindObjectOfType<GameManager>().LoadMainMenu();
    }
}
﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private static string nextLevel;

    public static void LoadLevel(string level)
    {
        nextLevel = level;
        SceneManager.LoadScene("LoadingScene");
    }

    private IEnumerator Start()
    {
        GameManager.SetGameState(GameState.Loading);

        var loadingRunner = GameObject.Find("LoadingRunner");
        var loadingFinish = GameObject.Find("LoadingFinish");
        while (true)
        {
            loadingRunner.transform.Translate(Time.deltaTime * 500 * Vector3.right);
            yield return null;
            if (loadingRunner.transform.position.x > loadingFinish.transform.position.x)
            {
                break;
            }
        }

        if (string.IsNullOrEmpty(nextLevel))
        {
            SceneManager.LoadScene("MainMenu");
            yield break;
        }

        AsyncOperation loading = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);

        while (!loading.isDone)
        {
            yield return null;
        }

        var canvasGroup = gameObject.GetComponent<CanvasGroup>();
        for (var i = 1f; i >= -0.05f; i -= 0.05f)
        {
            canvasGroup.alpha = i;
            yield return new WaitForSeconds(0.05f);
        }

        nextLevel = null;
        SceneManager.UnloadSceneAsync("LoadingScene");
    }
}
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "Data", menuName = "Objects/PlatformData", order = 1)]
    public class PlatformData : ScriptableObject
    {
        public Sprite startSprite;
        public Sprite middleSprite;
        public Sprite endSprite;
        public int length;
        public Vector3 position;
    }
}
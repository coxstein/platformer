using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "Data", menuName = "Objects/MultiplePlatformData", order = 1)]
    public class PlatformDataBundle : ScriptableObject
    {
        public List<PlatformData> platformDataBundle = new List<PlatformData>();
    }
}
using UnityEngine;
using Random = UnityEngine.Random;

public class SurfacePlatformController : MonoBehaviour
{
    [SerializeField] private SurfaceEffector2D surfaceEffector2D;
    [SerializeField] private float timeToChangeDirection = 1f;

    private void Start()
    {
        InvokeRepeating(nameof(ChangeDirectionInSeconds), 0, timeToChangeDirection);
    }

    private void ChangeDirectionInSeconds()
    {
        var direction = Random.Range(0, 2) == 0 ? -1 : 1;
        surfaceEffector2D.speed *= direction;
    }
}